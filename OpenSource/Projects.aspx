﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OpenSource.Master" AutoEventWireup="true" CodeBehind="Projects.aspx.cs" Inherits="OpenSource.Projects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <title>OpenSource - Privacy Statement</title>
    <style type="text/css">
        #main-content { width: 566px; }
        #main-content h2 { margin-left: 15px; }
        #main-content #div_content table
        {
            border: 0px solid blue;
            /* width: 535px; */
        }
        #main-content #div_content table ul,
        #main-content #div_content table ol,
        #main-content #div_content table dl
        {
            margin: 5px 15px 0px 10px;
        }
        .no_bullet
        {
            list-style-image: none;
        }
        #main-content #div_content .table_2
        {
            margin-left: 30px;
        }
        #main-content #div_content .table_2 td
        {
            padding-bottom: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <!-- BEGIN SUB-HEADER -->
    <div id="sub-header">
        <div id="sub-header-inside">
            <!-- BEGIN SUB HEADER BANNER -->
            <a href="Default.aspx">
                <img src="Images/sub_header_small.png"
                     alt="Collaborative Computing Solutions"
                     height="77"
                     title="Collaborative Computing Solutions"
                     width="566"
                     />
            </a> 
            <!-- END SUB HEADER BANNER -->
        </div>
    </div>
    <!-- END SUB-HEADER -->

    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
        <div id="page-title">
            <h1>Projects</h1>
        </div>

        <div id="div_content">
                <%--OnItemCreated="RadListView1_ItemCreated"--%>
            <telerik:RadListView ID="RadListView1" runat="server"
                DataSourceID="XmlDataSource_Projects"
                ItemPlaceHolderID="ListViewContainer"
                OnItemDataBound="RadListView1_ItemDataBound"
                >
                <LayoutTemplate>
                    <asp:PlaceHolder ID="ListViewContainer" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="div_listview_item">
                        <h2><%# Eval("Title") %></h2>
                        <p>
                            <%# Eval("Description") %>
                        </p>
                        <hr />
                        <p>
                            <table cellpadding="0" cellspacing="0" border="1">
                                <colgroup>
                                    <col width="160px;" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>Development Status:</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="no_bullet">
                                                <li><%# Eval("DevelopmentStatus") %></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>Environment:</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="no_bullet">
                                                <li><%# Eval("Environment") %></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>Intended Audience:</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="no_bullet">
                                                <li><%# Eval("IntendedAudience") %></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>License:</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="no_bullet">
                                                <li><%# Eval("License") %></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>Operating System:</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="no_bullet">
                                                <li><%# Eval("OperatingSystem") %></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul>
                                                <li>Program Language(s):</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="no_bullet">
                                                <li><%# Eval("ProgrammingLanguages") %></li>
                                            </ul>
                                        </td>
                                    </tr>
                                   <tr>
                                        <td>
                                            <ul>
                                                <li>Topic:</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="no_bullet">
                                                <li><%# Eval("Topic") %></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </p>
                        <hr />
                        <p>
                            <table class="table_2" cellpadding="0" cellspacing="0" border="1">
                                <tbody>
                                    <tr>
                                        <td>
                                            <b>Author:</b>
                                            <br />
                                            <%# Eval("Author") %>
                                        </td>
                                        <td>
                                            <b>Secondary Author(s):</b>
                                            <br />
                                            <%# Eval("SecondaryAuthors") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Homepage Link:</b>
                                            <br />
                                            <asp:Label ID="lblHomePageLink" runat="server"
                                                Text='<%# Eval("HomePageLink") %>'
                                                />
                                        </td>
                                        <td>
                                            <b>Demo Link:</b>
                                            <br />
                                            <%# Eval("DemoLink") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Download Link:</b>
                                            <br />
                                            <%# Eval("DownloadLink") %>
                                        </td>
                                        <td>
                                            <b>Send Questions To:</b>
                                            <br />
                                            <%# Eval("QuestionsTo") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Screen Shots:</b>
                                            <br />
                                            <%# Eval("ScreenShots") %>
                                        </td>
                                        <td>
                                            <b>Responsible Department:</b>
                                            <br />
                                            <asp:Label ID="lblDepartment" runat="server"
                                                Text='<%# Eval("Department") %>'
                                                />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Documentation:</b>
                                            <br />
                                            <%# Eval("Documentation") %>
                                        </td>
                                        <td>
                                            <b>Latest Product Version:</b>
                                            <br />
                                            <%# Eval("ProductVersion") %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </p>
                    </div>
                </ItemTemplate>
            </telerik:RadListView>

        </div>
        <!-- END DIV_CONTENT -->

    </div>
    <!-- END MAIN CONTENT -->

    <asp:XmlDataSource ID="XmlDataSource_Projects" runat="server"
        DataFile="~/App_Data/Projects.xml"
        XPath="/Items/Item[@ID='{0}']"
        />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphSubContent" runat="server">
</asp:Content>
