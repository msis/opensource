﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OpenSource.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OpenSource.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <title>OpenSource - Home</title>
    <link rel="stylesheet" href="../Styles/RadPanelBar.css" type="text/css" media="screen" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">

    <!-- BEGIN SUB-HEADER -->
    <div id="sub-header">
        <div id="sub-header-inside">
            <!-- BEGIN SUB HEADER BANNER -->
            <a href="Default.aspx">
                <asp:Image ID="imgSubHeaderLarge" runat="server"
                    AlternateText="Virginia Tech - Collaborative Computing Solutions"
                    Height="180"
                    ImageUrl="~/Images/sub_header_large.png"
                    Width="566"
                    ToolTip="Virginia Tech - Collaborative Computing Solutions"
                    />
            </a>
            <!-- END SUB HEADER BANNER -->
        </div>
    </div>
    <!-- END SUB-HEADER -->

    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
        <div>
            <center>
                <asp:Image ID="imgCCSlogo" runat="server"
                    AlternateText="OpenSource"
                    CssClass="css_img_logo"
                    Height="64"
                    ImageUrl="~/Images/OpenSource/OpenSource_Logo_450x64.png"
                    Width="400"
                    ToolTip="OpenSource"
                    />
            </center>
        </div>
        <div id="intro">
            <p>
                <h2>
                    Welcome to the Virginia Tech Collaborative Computing Solution's Open Source Software
                </h2>
            </p>
            <p>
                This site is provided by the Virginia Tech Collaborative Computing Solutions (CCS) department
                as a free service to share software we've written with the open source, university, and Internet
                communities.
            </p>
            <p>
                All of the software offered from this site was developed by Faculty and Staff of
                Virginia Tech.  We would like to keep track of who, when, and how often our software is downloaded. However, the
                information we gather during the registration process will be used only for our records. We WILL
                NOT sell, distribute, or share your information with anyone. You can find our more detailed privacy
                policy by clicking <a href="PrivacyStatement.aspx">here</a>.
                <br />
                <br />
                Software available from this site is free. You can redistribute it and/or modify it under the terms
                of the <a target="_blank" href="http://www.gnu.org/copyleft/gpl.html">GNU General Public License</a>.
            </p>
        </div>
    </div>
    <!-- END MAIN CONTENT -->

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphSubContent" runat="server">

    <!-- BEGIN SUB-CONTENT -->
    <div id="sub-content">

        <!-- BEGIN SUB-SECTION [You can repeat this division as often as desired.] -->
        <div class="sub-section">
            <h2>What's New</h2> 

            <telerik:RadListView ID="RadListView1" runat="server"
                DataSourceID="XmlDataSource_WhatsNew"
                ItemPlaceHolderID="ListViewContainer"
                >
                <LayoutTemplate>
                    <asp:PlaceHolder ID="ListViewContainer" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <ul>
                        <li>
                            <h4>
                                <a href='<%# Eval("Url") %>'
                                   target='<%# Eval("Target") %>'
                                   title='<%# Eval("Title") %>'><%# Eval("Title") %></a>
                            </h4>
                            <%# Eval("Description") %>
                            <br />
                        </li>
                    </ul>
                </ItemTemplate>
            </telerik:RadListView>
        </div>
        <!-- END SUB-SECTION -->

        <!-- BEGIN SUB-SECTION -->
        <div class="sub-section">
            <h2>Statistics</h2>
            <div class="ccs_div_counter">
                <img src="CounterImage.aspx" runat="server"
                     alt="Website Views"
                     class="ccs_img_counter"
                     id="imgCounter"
                     name="imgCounter"
                     title="Website Views"
                     />
                <p>
                    views of this website since January 23, 2002
                </p>
            </div>
        </div>
        <!-- END SUB-SECTION -->

    </div>
    <!-- END SUB-CONTENT -->

    <asp:XmlDataSource ID="XmlDataSource_WhatsNew" runat="server"
        DataFile="~/App_Data/WhatsNew.xml"
        XPath="/Items/Item[@Active='true']"
        />

    <script type="text/javascript">
        function pageLoad() {
            var oDiv = document.getElementById("main-content");
            if (oDiv) {
                oDiv.style.width = "405px";
            }
        }
    </script>

</asp:Content>
