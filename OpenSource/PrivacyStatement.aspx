﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OpenSource.Master" AutoEventWireup="true" CodeBehind="PrivacyStatement.aspx.cs" Inherits="OpenSource.PrivacyStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <title>OpenSource - Privacy Statement</title>
    <style type="text/css">
        #main-content { width: 566px; }
        #main-content h2 { margin-left: 15px; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <!-- BEGIN SUB-HEADER -->
    <div id="sub-header">
        <div id="sub-header-inside">
            <!-- BEGIN SUB HEADER BANNER -->
            <a href="Default.aspx">
                <img src="Images/sub_header_small.png"
                     alt="Collaborative Computing Solutions"
                     height="77"
                     title="Collaborative Computing Solutions"
                     width="566"
                     />
            </a> 
            <!-- END SUB HEADER BANNER -->
        </div>
    </div>
    <!-- END SUB-HEADER -->

    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
        <div id="page-title">
            <h1>Privacy Statement</h1>
        </div>

        <div id="div_content">
            <p>
                Virginia Tech has created this privacy statement in order demonstrate our firm commitment to privacy.
                The following discloses the information gathering and dissemination practices for this web site.
            </p>
            <h2>Information Automatically Logged</h2>
            <p>
                The server collects the IP address, date, time, page accessed, browser type, and the referring page
                if present.  We use your IP address to help diagnose problems with our server and to administer our
                Web page.  The date and time are used to track the load on the server and peak usage times.  We record
                the browser type to monitor market penetration of various web browsers so we can better determine what
                Internet technologies we may utilize in the design of our pages.  We use page referrer data--that is,
                information about the web page that pointed you to our page--to determine to what extent our page is
                referenced by other resources on the web.  These data may be used to preserve the integrity of our
                computing resources.
            </p>
            <p>
                This data may be used to preserve the integrity of our computing resources.
            </p>
            <h2>Personal Information</h2>
            <p>
                This site collects your name, company, and email address.
            </p>
            <p>
                We use this information to gauge interest in our software and also to have the ability to notify the
                appropriate people of software upgrades/fixes.
            </p>
            <p>
                All forms on this site collect information exclusively for the stated purpose of the form.
            </p>
            <p>
                We do not share any personal information with any third parties nor do we use any personal information
                for any purposes beyond those stated here.
            </p>
            <h2>Links to Virginia Tech Sites</h2>
            <p>
                This site contains links to other Virginia Tech pages.  The privacy practices of other pages may vary
                with the purposes of the page.  Consult the privacy statement on each page.
            </p>
            <h2>Links to External Sites</h2>
            <p>
                This site contains links to other sites.  Virginia Tech is not responsible for the privacy practices or
                the content of such web sites.
            </p>
            <h2>Security</h2>
            <p>
                This site has security measures in place to protect the loss, misuse, and alteration of the information
                under our control.  Log file access is restricted to system administrators while stored on the server.
                Log files are rotated regularly and archived in a secure location.
            </p>
            <p>
                Users should also consult Virginia Tech's policy on
                <a href="http://www.vt.edu/about/acceptable-use.html" target="_blank">Acceptable Use</a>.
            </p>
            <p>
                Virginia Tech complies with all statutory and legal requirements with respect to access to information.
            </p>
            <h2>Contact Information</h2>
            <p>
                If you have any questions about this privacy statement, the practices of this site, or your dealings
                with this site, you can <a href="mailto:hokiess@vt.edu">Contact Us</a>.
            </p>
        </div>
        <!-- END DIV_CONTENT -->

    </div>
    <!-- END MAIN CONTENT -->

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphSubContent" runat="server">
</asp:Content>
