﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OpenSource.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="OpenSource.Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <style type="text/css">
        #main-content
        {
            width: 566px;
        }

        .button_register
        {
            margin: 5px 0px 0px 0px;
        }

        .captcha_image
        {
            margin: 10px 0px 5px 0px;
        }

        .div_registration
        {
            margin: 0px 10px 0px 10px;
            width: 550px;
        }

        #table_registration
        {
            border: 0px solid blue;
            font-size: 90%;
            margin: 15px;
            width: 95%;
        }

        #table_registration h1
        {
            margin: 0px 0px 10px 0px;
        }

        #table_user_info
        {
            border: 0px solid red;
            font-size: 100%;
        }

        #table_user_info td
        {
            padding: 3px;
        }

        .nowrap
        {
            white-space: nowrap;
        }
    </style>

     <script type="text/javascript">
          //function pageLoad() {
          //     //Disable autoComplete
          //     $get("< %= txtRadCaptcha.ClientID %>").setAttribute("autoComplete", "off");
          //}
     </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <!-- BEGIN SUB-HEADER -->
    <div id="sub-header">
        <div id="sub-header-inside">
            <!-- BEGIN SUB HEADER BANNER -->
            <a href="Default.aspx">
                <img src="Images/sub_header_small.png"
                     alt="Collaborative Computing Solutions"
                     height="77"
                     title="Collaborative Computing Solutions"
                     width="566"
                     />
            </a> 
            <!-- END SUB HEADER BANNER -->
        </div>
    </div>
    <!-- END SUB-HEADER -->

    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
	    <input type="hidden" id="intProjectId" name="intProjectId" value="" />

        <div class="div_registration">
            <table id="table_registration" cellpadding="0" cellspacing="0" border="1">
                <tr>
                    <td>
		                <h1>
                            Download Registration:&nbsp;&nbsp;
                            <asp:Label ID="lblProject" runat="server" Text="" />
		                </h1>
                        <p>
			                Thank you for taking the time to give us a little information about yourself.
			                An email will be sent to the address you enter below, with a link to the
			                software package you are registering for.
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<form id="frmRegistration" name="frmRegistration" method="post" action="ProcessRegistration.aspx">--%>
			                <table id="table_user_info" cellpadding="5" cellspacing="3" border="1">
				                <tr>
					                <td class="nowrap">
						                Email Address:
					                </td>
					                <td>
                                        <%--<input type="text" id="txtEmailAddress" name="txtEmailAddress" maxlength="50" size="60" value="" />--%>
                                        <asp:TextBox ID="txtEmailAddress" runat="server"
                                            MaxLength="50"
                                            Width="200"
                                            Text=""
                                            />
					                </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfvEmailAddress" runat="server"
                                            ControlToValidate="txtEmailAddress"
                                            ErrorMessage="RequiredFieldValidator"
                                            Display="Static"
                                            Text="* Email Address is required"
                                        />
                                    </td>
				                </tr>
				                <tr>
					                <td>
						                Name:
					                </td>
					                <td>
                                        <%--<input type="text" id="txtName" name="txtName" maxlength="50" size="60" value="" />--%>
                                        <asp:TextBox ID="txtName" runat="server"
                                            MaxLength="50"
                                            Width="200"
                                            Text=""
                                            />
					                </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtEmailAddress"
                                            ErrorMessage="RequiredFieldValidator"
                                            Display="Static"
                                            Text="* Email Address is required"
                                        />
                                    </td>
				                </tr>
				                <tr>
					                <td>
						                Company:
					                </td>
					                <td>
                                        <%--<input type="text" id="txtCompany" name="txtCompany" maxlength="100" size="60" value="" />--%>
                                        <asp:TextBox ID="txtCompany" runat="server"
                                            MaxLength="50"
                                            Width="200"
                                            Text=""
                                            />
					                </td>
				                </tr>
				                <tr>
					                <td valign="top">
						                Code:
					                </td>
					                <td>
                                        <input type="text" id="txtRadCaptcha" name="txtRadCaptcha" maxlength="5" size="10" value="" />
						                &nbsp;&nbsp;Enter the code shown below.
					                </td>
				                </tr>
                                <tr>
					                <td>
                                        &nbsp;
					                </td>
					                <td>
                                        <telerik:RadCaptcha ID="RadCaptcha1" runat="server"
                                            CaptchaLinkButtonText="Generate New Image"
                                            EnableRefreshImage="true"
                                            ValidatedTextBoxID="txtRadCaptcha"
                                            >
                                            <CaptchaImage ImageCssClass="captcha_image" RenderImageOnly="true" TextLength="5" />
                                        </telerik:RadCaptcha>
					                </td>
                                </tr>
                                <tr>
					                <td>
                                        &nbsp;
					                </td>
					                <td>
                                        <%--<input type="submit" class="button_register" id="btnSubmit" name="btnSubmit" size="60" value="Register" />--%>
                                        <asp:Button ID="btnSubmit" runat="server"
                                            OnClick="btnSubmit_Click"
                                            PostBackUrl="~/Registration.aspx"
                                            Text="Register"
                                            UseSubmitBehavior="False"
                                            />
                                    </td>
                                </tr>
                            </table>
                	    <%--</form>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
			                The information we gather during the registration process will be used only for our records.
			                We WILL NOT sell, distribute, or share your information with anyone.  You can find our more
			                detailed privacy policy by clicking
                            <a href="PrivacyStatement.aspx">here</a>.
		                </p>
                        <p>
                            The software available from this web site is free. You can redistribute it and/or modify it
                            under the terms of the
			                <a href="http://www.gnu.org/copyleft/gpl.html" target="_blank">GNU General Public License</a>.
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <!-- END DIV_REGISTRATION -->

    </div>
    <!-- END MAIN CONTENT -->
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphSubContent" runat="server">
</asp:Content>
