﻿using System;

namespace OpenSource
{
    public partial class CounterImage : System.Web.UI.Page
    {
        // ---------------------------------------------------------------------
        // Control Events:
        // ---------------
        //     Page_Load()
        // ---------------------------------------------------------------------

        #region .  Page_Load()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Load()
        //
        //  Description:  This routine is called when the page loads.  It calls
        //                Counter.GetCounter() to get the current site view count
        //                and passes it to Counter.CreateBitmapImage() to turn it
        //                into a bitmap.  The bitmap created is saved directly to
        //                the Response stream.  The ContentType for this page is
        //                set to "image/gif" so the bitmap is automatically drawn
        //                on the page.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int intCounter = Convert.ToInt32(Counter.GetCounter());
                Counter.CreateBitmapImage(intCounter.ToString("#,##0"));
            }
            catch
            { }

        }   // Page_Load()
        #endregion

    } // class CounterImage

}   // namespace OpenSource
