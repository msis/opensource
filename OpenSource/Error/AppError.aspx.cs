﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace OpenSource.Error
{
    public partial class AppError : System.Web.UI.Page
    {
        #region .  Page_Load()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Load()
        //
        //  Description:  This routine is called when the page is being loaded.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    // Set the background image for the "page-area-background" div
                    // to the two-column format for the content area.
                    HtmlContainerControl myObject;
                    myObject = (HtmlContainerControl)Master.FindControl("page_area_background");
                    if (myObject != null)
                    {
                        myObject.Style.Remove("background");
                        myObject.Style.Add("background", "#F5F5EB url(../Images/pgarea_vnav_subc_2_col.gif) repeat-y 100% 0;");
                        myObject.Style.Remove("width");
                    }

                    if (Request.QueryString["msg"] != null)
                    {
                        lblErrorMessage.Text = Request.QueryString["msg"];
                    }

                    // Get the formatted error message and check to email it to the appropriate destination.
                    Utilities.SendEmail(_GetErrorAsString(), Constants.DEFAULT_APPSETTING_EMAIL_SUBJECT_MESSAGE);
                }
            }
            catch { }

        }   // Page_Load()
        #endregion


        // ---------------------------------------------------------------------
        // Private Methods:
        // ----------------
        //     _GetErrorAsString()
        // ---------------------------------------------------------------------

        #region .  _GetErrorAsString()  .
        //----------------------------------------------------------------------
        // Routine:      _GetErrorAsString()
        //
        // Description:  Returns the error contents as a nicely formatted string.
        //
        // Parameters:   None
        //
        // Session:      Exception   - The Exception object.
        //
        //               ErrorClass  - The name of the class that the Exception
        //                             occurred in.
        //
        //               ErrorMethod - The name of the method that the Exception
        //                             occurred in.
        //
        // Returns:      String - the error contents formatted nicely.
        //----------------------------------------------------------------------
        private string _GetErrorAsString()
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                Exception ex = Server.GetLastError().GetBaseException();

                sb.Append(String.Format("Error in: {0}, ",      Request.Url.ToString()));
                sb.Append(String.Format("Error Message: {0}, ", ex.Message.ToString()));
                sb.Append(String.Format("Stack Trace: {0}",     ex.StackTrace.ToString()));

            }
            catch (Exception ex)
            {
                // Pass the error up the chain.
                throw ex;
            }

            return sb.ToString();

        }   // _GetErrorAsString()
        #endregion


    }   // class AppError

}   // namespace OpenSource
