﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OpenSource.Master" AutoEventWireup="true" CodeBehind="AppError.aspx.cs" Inherits="OpenSource.Error.AppError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <style type="text/css">
        /* body      { font: 8pt/12pt verdana; } */
        #div_error a:link    { color: red; }
        #div_error a:visited { color: maroon; }
        #div_error h1        { font: 13pt/15pt verdana; padding-bottom: 20px; }
        #div_error h2        { font: 8pt/12pt verdana; }
        #div_error hr        { margin: 20px 0px 20px 0px; }
        #div_error li        { padding-bottom: 10px; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div id="div_error">
        <table width="500" cellspacing="10" border="0">
            <tr>
                <td>
                    <h1>OpenSource - Application Error</h1>
                    The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.
                    <hr/>
                    We're sorry but the operation you requested has generated an error.
                    <br />
                    <br />
                    The web site administrator has been made aware of this situation.
                    <br />
                    <br />
                    If you  need additional assistance, please contact your System
                    Administrator and provide the following information:
                    <br />
                    <br />
                    <ul>
                        <li>Can you reproduce this error?</li>
                        <li>Detailed steps to reproduce the error.</li>
                        <li>Any additional information to assist in resolving this problem.</li>
                    </ul>
                    <hr/>
                    <h2><asp:Label ID="lblErrorMessage" runat="server" Text="" /></h2>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphSubContent" runat="server">
</asp:Content>
