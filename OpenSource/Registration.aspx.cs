﻿using System;
using System.Web.UI.HtmlControls;

namespace OpenSource
{
    public partial class Registration : System.Web.UI.Page
    {
        // ---------------------------------------------------------------------
        // Private Properties:
        // -------------------
        //     _Company
        //     _EmailAddress
        //     _ID
        //     _Name
        // ---------------------------------------------------------------------

        #region .  Private Properties  .

        private string _Company
        {
            get
            {
                return (ViewState["_Company"] == null
                  ? null
                  : ViewState["_Company"].ToString());
            }
            set { ViewState["_Company"] = value; }
        }

        private string _EmailAddress
        {
            get
            {
                return (ViewState["_EmailAddress"] == null
                  ? null
                  : ViewState["_EmailAddress"].ToString());
            }
            set { ViewState["_EmailAddress"] = value; }
        }

        private string _ID
        {
            get
            {
                return (ViewState["_ID"] == null
                  ? null
                  : ViewState["_ID"].ToString());
            }
            set { ViewState["_ID"] = value; }
        }

        private string _Name
        {
            get
            {
                return (ViewState["_Name"] == null
                  ? null
                  : ViewState["_Name"].ToString());
            }
            set { ViewState["_Name"] = value; }
        }

        #endregion


        // ---------------------------------------------------------------------
        // Control Events:
        // ---------------
        //     Page_Load()
        //     btnSubmit_Click()
        // ---------------------------------------------------------------------

        #region .  Page_Load()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Load()
        //
        //  Description:  This routine is called when the page first loads.  It
        //                changes the "page-area-background" <div> image to the
        //                two-column image.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    // Set the background image for the "page-area-background" div
                    // to the two-column format for the content area.
                    HtmlContainerControl myObject;
                    myObject = (HtmlContainerControl)Master.FindControl("page_area_background");
                    if (myObject != null)
                    {
                        myObject.Style.Remove("background");
                        myObject.Style.Add("background", "#F5F5EB url(../Images/pgarea_vnav_subc_2_col.gif) repeat-y 100% 0;");
                    }

                    // Get the project ID from the QueryString and store in private property.
                    if (Request.QueryString["ID"] == null)
                    {
                        throw new Exception("Project ID is missing.");
                    }
                    else
                    {
                        this._ID = Request.QueryString["ID"];
                    }
                }
                else
                {
                    this._EmailAddress = txtEmailAddress.Text;
                    this._Company = txtCompany.Text;
                    this._Name = txtName.Text;
                }
            }
            catch { }

        }   // Page_Load()
        #endregion


        #region .  btnSubmit_Click()  .
        // ---------------------------------------------------------------------
        //  Routine:      btnSubmit_Click()
        //
        //  Description:  This routine is called when the Register button is
        //                clicked.  It calls the ProcessRegistration page and
        //                passes in the project ID in the QueryString.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Session["Company"] = this._Company;
                Session["EmailAddress"] = this._EmailAddress;
                Session["ID"] = this._ID;
                Session["Name"] = this._Name;

                Response.Redirect("~/ProcessRegistration.aspx", false);
            }
            catch { }

        }   // btnSubmit_Click()
        #endregion


    }   // class Registration

}   // namespace OpenSource
