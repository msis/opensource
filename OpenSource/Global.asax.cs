﻿using System;

namespace OpenSource
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // Get the current site view count, increment it by 1, and
            // write it back to the Text file.  This is done only once
            // per visit to this web site.
            int intCounter = Convert.ToInt32(Counter.GetCounter()) + 1;
            Counter.SetCounter(intCounter.ToString());
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}