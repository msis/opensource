﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OpenSource
{
    public partial class OpenSource : System.Web.UI.MasterPage
    {
        #region .  Page_Load()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Load()
        //
        //  Description:  This routine sets up the home page.  The Next Meeting
        //                information is read from an XML data file, and the
        //                copyright year in the footer is filled in.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // -------------------------------------------------------------
                //  Get environment-specific information.
                // -------------------------------------------------------------

                string strEnvironment = AppSettings.Environment;
                if (String.IsNullOrEmpty(strEnvironment))
                {
                    strEnvironment = Constants.DEFAULT_APPSETTING_ENVIRONMENT;
                }
                string strDomain = String.Empty;
                switch (strEnvironment.ToLower())
                {
                    case "dev":
                        strDomain = "-dev";
                        break;

                    case "test":
                        strDomain = "-test";
                        break;

                    case "prod":
                        break;
                }

                lnkCCSWebSite.NavigateUrl    = String.Format("http://ccs.w2k{0}.vt.edu",           strDomain);
                lnkWindowsDomain.NavigateUrl = String.Format("http://windowsdomain.w2k{0}.vt.edu", strDomain);



                // -------------------------------------------------------------
                //  Footer:  Get links, version, last update, and copyright year.
                // -------------------------------------------------------------
                lnkAcceptableUsePolicy.NavigateUrl   = AppSettings.Link_AcceptableUsePolicy;
                lnkAccessibility.NavigateUrl         = AppSettings.Link_Accessibility;
                lnkBookstore.NavigateUrl             = AppSettings.Link_Bookstore;
                lnkContactUs.NavigateUrl             = AppSettings.Link_ContactUs;
                lnkEqualOpportunity.NavigateUrl      = AppSettings.Link_EqualOpportunity;
                lnkGivingToVirginiaTech.NavigateUrl  = AppSettings.Link_GivingToVirginiaTech;
                lnkJobsAtVirginiaTech.NavigateUrl    = AppSettings.Link_JobsAtVirginiaTech;
                lnkPrinciplesOfCommunity.NavigateUrl = AppSettings.Link_PrinciplesOfCommunity;
                lnkPrivacyStatement.NavigateUrl      = AppSettings.Link_PrivacyStatement;
                lnkWebsiteFeedback.NavigateUrl       = AppSettings.Link_WebsiteFeedback;

                lblAppVersion.Text                   = AppSettings.Label_AppVersion;

                string   strLastUpdated              = AppSettings.Label_LastUpdated;
                DateTime datLastUpdated              = DateTime.Parse(strLastUpdated);
                lblLastUpdated.Text                  = datLastUpdated.ToString("dddd, MMMM d, yyyy");

                // Fill in the copyright year.
                lblCopyrightYear.Text = DateTime.Now.ToString("yyyy");
            }
            catch { }

        }   // Page_Load()
        #endregion

    }   // class OpenSource

}   // namespace OpenSource
