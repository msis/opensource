﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace OpenSource
{
    public partial class Projects : System.Web.UI.Page
    {
        // ---------------------------------------------------------------------
        // Control Events:
        // ---------------
        //     Page_Init()
        //     Page_Load()
        // ---------------------------------------------------------------------

        #region .  Page_Init()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Init()
        //
        //  Description:  This routine is called when the page first loads.  It
        //                changes the "page-area-background" <div> image to the
        //                two-column image.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                string strID = Request.QueryString["ID"];
                if (String.IsNullOrEmpty(strID))
                {
                    strID = "1";
                }
                XmlDataSource_Projects.XPath = String.Format(XmlDataSource_Projects.XPath, strID);
            }
            catch { }

        }   // Page_Init()
        #endregion


        #region .  Page_Load()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Load()
        //
        //  Description:  This routine is called when the page first loads.  It
        //                changes the "page-area-background" <div> image to the
        //                two-column image.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    // Set the background image for the "page-area-background" div
                    // to the two-column format for the content area.
                    HtmlContainerControl myObject;
                    myObject = (HtmlContainerControl)Master.FindControl("page_area_background");
                    if (myObject != null)
                    {
                        myObject.Style.Remove("background");
                        myObject.Style.Add("background", "#F5F5EB url(../Images/pgarea_vnav_subc_2_col.gif) repeat-y 100% 0;");
                    }
                }
            }
            catch { }

        }   // Page_Load()
        #endregion


        #region .  RadListView1_ItemCreated()  .
        // ---------------------------------------------------------------------
        //  Routine:      RadListView1_ItemCreated()
        //
        //  Description:  This routine is called when an item is created in the
        //                RadListView control.  Some items are specific to the
        //                environment (i.e., DEV, TEST or PROD).  Replace the
        //                proper environment portion of the various controls.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void RadListView1_ItemDataBound(object sender, RadListViewItemEventArgs e)
        {
            try
            {
                if (e.Item is RadListViewDataItem)
                {
                    RadListViewDataItem oItem = (RadListViewDataItem)e.Item;

                    //  Get environment-specific information.
                    string strDomain = (AppSettings.Environment == "prod") ? "" : "-" + AppSettings.Environment;

                    Label lblDepartment = (Label)oItem.FindControl("lblDepartment");
                    lblDepartment.Text = String.Format(lblDepartment.Text, strDomain);

                    Label lblHomePageLink = (Label)oItem.FindControl("lblHomePageLink");
                    lblHomePageLink.Text = String.Format(lblHomePageLink.Text, strDomain);

                }
            }
            catch { }

        }   // RadListView1_ItemDataBound()
        #endregion


    }   // class Projects

}   // namespace OpenSource
