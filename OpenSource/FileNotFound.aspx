﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OpenSource.Master" AutoEventWireup="true" CodeBehind="FileNotFound.aspx.cs" Inherits="OpenSource.FileNotFound" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <style type="text/css">
        /* body      { font: 8pt/12pt verdana; } */
        #div_error h1        { font: 13pt/15pt verdana; padding-bottom: 10px; }
        #div_error h2        { font: 8pt/12pt verdana; }
        #div_error hr        { margin: 20px 0px 20px 0px; }
        #div_error a:link    { color: red; }
        #div_error a:visited { color: maroon; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div id="div_error">
        <table width="500" cellspacing="10" border="0">
            <tr>
                <td>
                    <h1>The Web site cannot be found</h1>
                    The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.
                    <hr/>
                    <p>Please try the following:</p>
                    <ul>
                        <li>
                            Make sure that the Web site address displayed in the address bar of your browser is spelled
                            and formatted correctly.
                        </li>
                        <li>
                            If you reached this page by clicking a link, contact the Web site administrator to alert them
                            that the link is incorrectly formatted.
                        </li>
                        <li>
                            Click the <a href="javascript:history.back(1)">Back</a> button to try another link.
                        </li>
                    </ul>
                    <hr/>
                    <h2>
                        HTTP Error 404 - File or directory not found.
                    </h2>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphSubContent" runat="server">
</asp:Content>
