﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;

namespace OpenSource
{
    public class Utilities
    {
        #region .  Utilities() constructor  .
        public Utilities()
        {
        }
        #endregion


        // ---------------------------------------------------------------------
        // Public Methods:
        // ---------------
        //     ReplaceText()
        //     SendEmail()
        // ---------------------------------------------------------------------

        #region.  ReplaceText()  .
        // ---------------------------------------------------------------------
        //  Routine:      ReplaceText()
        //
        //  Description:  This routine replaces search a string and replaces
        //                all occurrences of the value in strMatch with the
        //                value in strReplaceWith.
        //
        //  Parameters:   strText        - String, the string to search in.
        //
        //                strMatch       - String, the string to search for.
        //
        //                strReplaceWith - String, the string to replace with.
        //
        //  Returns:      String, the string with all occurences of the match
        //                text replaced with the replace text.
        // ---------------------------------------------------------------------
        static string ReplaceText(string strText, string strMatch, string strReplaceWith)
        {
            string strResults = strText;

            try
            {
                // Use RegEx to replace the text.  This call also wants to know
                // the maximum number of times to replace this piece of text in
                // the entire string.
                Regex re = new Regex(strMatch);
                strResults = re.Replace(strText, strReplaceWith, 99, 0);
            }
            catch (Exception ex)
            {
                // Throw the error up the chain.
                throw new Exception(ex.Message, ex);
            }

            return strResults;

        }   // ReplaceText()
        #endregion


        #region .  SendEmail()  .
        //----------------------------------------------------------------------
        // Routine:      SendEmail()
        //
        // Description:  Sends an email to the address(es) specified in the
        //               web.config configuration file.
        //
        // Parameters:   strEmailBody - String, body of the email message.
        //
        //               strSubject   - String, additional information you want
        //                              to display in the email subject template.
        //
        // Returns:      Boolean - TRUE if the email was sent successfully (this
        //               really means if this routine can successfully send the
        //               email to the SMTP server, not if the SMTP server sends
        //               the email and the recipient recieves it); otherwise,
        //               FALSE.
        //----------------------------------------------------------------------
        public static bool SendEmail(string strEmailBody, string strSubject)
        {
            bool blnResult = false;

            try
            {
                int    intErrNum       = 0;
                string strEmailFrom    = AppSettings.Email_AddressFrom;
                string strEmailServer  = AppSettings.Email_Server;
                string strEmailSubject = AppSettings.Email_SubjectTemplate;
                string strEmailTo      = AppSettings.Email_AddressTo;
                string strEnvironment  = AppSettings.Environment;
                string strErrMsg       = String.Empty;

                // This setting in the configuration file determines if emails
                // actually get sent.  Even though you may have specified the
                // parameter /SEND_EMAIL, if this setting is false, the email
                // will NOT be sent.
                bool blnEmailEnabled = AppSettings.Email_IsEmailEnabled;

                // Is emailing enabled?
                if (blnEmailEnabled)
                {
                    // Make sure all the required parts are there before sending.
                    if (strEmailBody.Length == 0)
                    {
                        intErrNum = (int)Constants.EmailErrorCode.EmailMessageBlank;
                        strErrMsg = Constants.MSG_EMAIL_MESSAGE_BLANK;
                    }
                    else if (strEmailTo.Length == 0)
                    {
                        intErrNum = (int)Constants.EmailErrorCode.EmailAddressToBlank;
                        strErrMsg = Constants.MSG_EMAIL_ADDRESS_TO_BLANK;
                    }
                    else if (strEmailFrom.Length == 0)
                    {
                        intErrNum = (int)Constants.EmailErrorCode.EmailAddressFromBlank;
                        strErrMsg = Constants.MSG_EMAIL_ADDRESS_FROM_BLANK;
                    }
                    else if (strEmailSubject.Length == 0)
                    {
                        intErrNum = (int)Constants.EmailErrorCode.EmailSubjectBlank;
                        strErrMsg = Constants.MSG_EMAIL_SUBJECT_BLANK;
                    }
                    else if (strEmailServer.Length == 0)
                    {
                        intErrNum = (int)Constants.EmailErrorCode.EmailSmtpServerBlank;
                        strErrMsg = Constants.MSG_EMAIL_SMTP_SERVER_BLANK;
                    }

                    // Handle any error with the required parts.
                    if (!String.IsNullOrEmpty(strErrMsg))
                    {
                        string strMessage = String.Format("{0}|{1}",
                                                           intErrNum,
                                                           strErrMsg);

                        // Throw my custom error up the chain.
                        throw new Exception(strMessage);
                    }

                    // Replace fields in email subject.
                    strEmailSubject = Utilities.ReplaceText(strEmailSubject, "%FIELD_ENVIRONMENT%",     strEnvironment);
                    strEmailSubject = Utilities.ReplaceText(strEmailSubject, "%FIELD_COMPUTER%",        System.Environment.MachineName);
                    strEmailSubject = Utilities.ReplaceText(strEmailSubject, "%FIELD_SUBJECT_MESSAGE%", strSubject);

                    // All the required parts are present so construct the email.
                    MailMessage oMessage = new MailMessage();

                    // Check for multiple TO recipients.
                    string[] astrAddresses = strEmailTo.Split(';');
                    foreach (string strAddress in astrAddresses)
                    {
                        // Build the email TO address and add it to the message.
                        oMessage.To.Add(new MailAddress(strAddress));
                    }

                    // Add the remaining parts of the message.
                    oMessage.From    = new MailAddress(strEmailFrom);
                    oMessage.Subject = strEmailSubject;
                    oMessage.Body    = strEmailBody;

                    // Get the timeout, retries, and wait between retries.
                    int intNumRetries         = AppSettings.Email_Retries;
                    int intTimeout            = AppSettings.Email_Timeout;
                    int intWaitBetweenRetries = AppSettings.Email_WaitBetweenRetries;

                    // Send the email.
                    SmtpClient oClient = new SmtpClient(strEmailServer);
                    oClient.Timeout    = intTimeout;

                    for (int i = 0; i < intNumRetries; i++)
                    {
                        try
                        {
                            // Wait between attempts (only after the first attempt).
                            if (i > 0)
                                Thread.Sleep(intWaitBetweenRetries);

                            // Attempt to send the email.
                            oClient.Send(oMessage);

                            // Success.
                            blnResult = true;
                            break;
                        }
                        catch (System.Net.Mail.SmtpFailedRecipientsException ex)
                        {
                            // Failed to send to all recipients.
                            strErrMsg =
                                String.Format("{0}|{1}|{2}",
                                              Constants.RC_ERROR_NUM_EMAIL_FAILED_SEND_TO_ALL_RECIPIENTS,
                                              Constants.RC_ERROR_MSG_EMAIL_FAILED_SEND_TO_ALL_RECIPIENTS
                                                  + ""
                                                  + ex.InnerException == null ? "" : ex.InnerException.Message,
                                              ex.StackTrace);
                        }
                        catch (System.Net.Mail.SmtpFailedRecipientException ex)
                        {
                            // Failed to send to a single recipient.
                            strErrMsg =
                                String.Format("{0}|{1}|{2}",
                                              Constants.RC_ERROR_NUM_EMAIL_FAILED_SEND_TO_RECIPIENT,
                                              Constants.RC_ERROR_MSG_EMAIL_FAILED_SEND_TO_RECIPIENT
                                                  + ""
                                                  + ex.InnerException == null ? "" : ex.InnerException.Message,
                                              ex.StackTrace);
                        }
                        catch (System.Net.Mail.SmtpException ex)
                        {
                            // Other SMTP related error.
                            strErrMsg =
                                String.Format("{0}|{1}|{2}",
                                              Constants.RC_ERROR_NUM_EMAIL_FAILED_TO_SEND,
                                              Constants.RC_ERROR_MSG_EMAIL_FAILED_TO_SEND
                                                  + ""
                                                  + ex.InnerException == null ? "" : ex.InnerException.Message,
                                              ex.StackTrace);
                        }

                    }   // end of:  for()

                    // Check for error.
                    if (!String.IsNullOrEmpty(strErrMsg))
                    {
                        // Throw custom error up the chain.
                        throw new Exception(strErrMsg);
                    }

                }   // end of:  if (blnEmailEnabled)
            }
            catch (System.Net.Mail.SmtpFailedRecipientsException ex)
            {
                // Failed to send to all recipients
                string strErrMsg = String.Format("{0} - {1}",
                                                 Constants.MSG_EMAIL_FAILED_SEND_TO_ALL_RECIPIENTS,
                                                 ex.Message);

                // Throw Exception error up the chain.
                throw new Exception(strErrMsg);
            }
            catch (System.Net.Mail.SmtpFailedRecipientException ex)
            {
                string strErrMsg = String.Format("{0} - {1}",
                                                 Constants.MSG_EMAIL_FAILED_SEND_TO_RECIPIENT,
                                                 ex.Message);

                // Throw Exception error up the chain.
                throw new Exception(strErrMsg);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                // Other SMTP related error.
                string strErrMsg = String.Format("{0} - {1}",
                                                 Constants.MSG_EMAIL_FAILED_TO_SEND,
                                                 ex.Message);

                // Throw Exception error up the chain.
                throw new Exception(strErrMsg);
            }
            catch (Exception ex)
            {
                // Re-throw the unexpected error up the chain.
                throw ex;
            }

            return blnResult;

        }   // SendEmail()
        #endregion


    }   // class Utilities

}   // namespace OpenSource
