﻿using System;
using System.Configuration;

namespace OpenSource
{
    public static class AppSettings
    {
        // ---------------------------------------------------------------------
        // Public Properties:
        // ---------------------------------------------------------------------

        #region .  Public Properties  .

        // Environment Settings:
        public static string Environment                 = GetAppSetting(Constants.APPSETTING_KEY_ENVIRONMENT,                                Constants.DEFAULT_APPSETTING_ENVIRONMENT);

        // General Application Settings:
        public static string Label_AppName               = GetAppSetting(Constants.APPSETTING_KEY_APPLICATION_NAME,                           Constants.DEFAULT_APPSETTING_LABEL_APP_NAME);
        public static string Label_AppVersion            = GetAppSetting(Constants.APPSETTING_KEY_APPLICATION_VERSION,                        Constants.DEFAULT_APPSETTING_LABEL_APP_VERSION);
        public static string Label_LastUpdated           = GetAppSetting(Constants.APPSETTING_KEY_LAST_UPDATED,                               Constants.DEFAULT_APPSETTING_LABEL_LAST_UPDATED);

        // Counter Settings:
        public static string Counter_File                = GetAppSetting(Constants.APPSETTING_KEY_COUNTER_FILE,                               Constants.DEFAULT_APPSETTING_COUNTER_FILE);
        public static string Counter_FontName            = GetAppSetting(Constants.APPSETTING_KEY_COUNTER_FONT_NAME,                          Constants.DEFAULT_APPSETTING_COUNTER_FONT_NAME);
        public static string Counter_FontSize            = GetAppSetting(Constants.APPSETTING_KEY_COUNTER_FONT_SIZE,                          Constants.DEFAULT_APPSETTING_COUNTER_FONT_SIZE);


        // Email Settings:
        public static string Email_AddressFrom           = GetAppSetting(Constants.APPSETTING_KEY_EMAIL_ADDRESS_FROM,                         Constants.DEFAULT_APPSETTING_EMAIL_ADDRESS_FROM);
        public static string Email_AddressTo             = GetAppSetting(Constants.APPSETTING_KEY_EMAIL_ADDRESS_TO,                           Constants.DEFAULT_APPSETTING_EMAIL_ADDRESS_TO);
        public static bool   Email_IsEmailEnabled        = Convert.ToBoolean(GetAppSetting(Constants.APPSETTING_KEY_EMAIL_IS_EMAIL_ENABLED,   Constants.DEFAULT_APPSETTING_EMAIL_IS_EMAIL_ENABLED));
        public static int    Email_Retries               = Convert.ToInt32(GetAppSetting(Constants.APPSETTING_KEY_EMAIL_RETRIES,              Constants.DEFAULT_APPSETTING_EMAIL_RETRIES));
        public static string Email_Server                = GetAppSetting(Constants.APPSETTING_KEY_EMAIL_SERVER,                               Constants.DEFAULT_APPSETTING_EMAIL_SERVER);
        public static string Email_SubjectMessage        = GetAppSetting(Constants.APPSETTING_KEY_EMAIL_SUBJECT_MESSAGE,                      Constants.DEFAULT_APPSETTING_EMAIL_SUBJECT_MESSAGE);
        public static string Email_SubjectTemplate       = GetAppSetting(Constants.APPSETTING_KEY_EMAIL_SUBJECT_TEMPLATE,                     Constants.DEFAULT_APPSETTING_EMAIL_SUBJECT_TEMPLATE);
        public static int    Email_Timeout               = Convert.ToInt32(GetAppSetting(Constants.APPSETTING_KEY_EMAIL_TIMEOUT,              Constants.DEFAULT_APPSETTING_EMAIL_TIMEOUT));
        public static int    Email_WaitBetweenRetries    = Convert.ToInt32(GetAppSetting(Constants.APPSETTING_KEY_EMAIL_WAIT_BETWEEN_RETRIES, Constants.DEFAULT_APPSETTING_EMAIL_WAIT_BETWEEN_RETRIES));


        // Link Settings (Footer):
        public static string Link_AcceptableUsePolicy    = GetAppSetting(Constants.APPSETTING_KEY_LINK_ACCEPTABLE_USE_POLICY,                 Constants.DEFAULT_APPSETTING_LINK_ACCEPTABLE_USE_POLICY);
        public static string Link_Accessibility          = GetAppSetting(Constants.APPSETTING_KEY_LINK_ACCESSIBILITY,                         Constants.DEFAULT_APPSETTING_LINK_ACCESSIBILITY);
        public static string Link_Bookstore              = GetAppSetting(Constants.APPSETTING_KEY_LINK_BOOKSTORE,                             Constants.DEFAULT_APPSETTING_LINK_BOOKSTORE);
        public static string Link_ContactUs              = GetAppSetting(Constants.APPSETTING_KEY_LINK_CONTACT_US,                            Constants.DEFAULT_APPSETTING_LINK_CONTACT_US);
        public static string Link_EqualOpportunity       = GetAppSetting(Constants.APPSETTING_KEY_LINK_EQUAL_OPPORTUNITY,                     Constants.DEFAULT_APPSETTING_LINK_EQUAL_OPPORTUNITY);
        public static string Link_GivingToVirginiaTech   = GetAppSetting(Constants.APPSETTING_KEY_LINK_GIVING_TO_VIRGINIA_TECH,               Constants.DEFAULT_APPSETTING_LINK_GIVING_TO_VIRGINIA_TECH);
        public static string Link_JobsAtVirginiaTech     = GetAppSetting(Constants.APPSETTING_KEY_LINK_JOBS_AT_VIRGINIA_TECH,                 Constants.DEFAULT_APPSETTING_LINK_JOBS_AT_VIRGINIA_TECH);
        public static string Link_PrinciplesOfCommunity  = GetAppSetting(Constants.APPSETTING_KEY_LINK_PRINCIPLES_OF_COMMUNITY,               Constants.DEFAULT_APPSETTING_LINK_PRINCIPLES_OF_COMMUNITY);
        public static string Link_PrivacyStatement       = GetAppSetting(Constants.APPSETTING_KEY_LINK_PRIVACYS_TATEMENT,                     Constants.DEFAULT_APPSETTING_LINK_PRIVACY_STATEMENT);
        public static string Link_WebsiteFeedback        = GetAppSetting(Constants.APPSETTING_KEY_LINK_PRIVACYS_TATEMENT,                     Constants.DEFAULT_APPSETTING_LINK_WEBSITE_FEEDBACK);


        // Projects File Settings:
        public static string Projects_File               = GetAppSetting(Constants.APPSETTING_KEY_PROJECTS_FILE,                              Constants.DEFAULT_APPSETTING_PROJECTS_FILE);

        
        public static string Skin = "";

        #endregion


        // ---------------------------------------------------------------------
        // Public Methods:
        // ---------------
        //     GetAppSetting()
        // ---------------------------------------------------------------------

        #region .  GetAppSetting()  .
        // --------------------------------------------------------------------
        //  Routine:      GetAppSetting()
        //
        //  Description:  This routine returns the value of application setting
        //                (in the web.config file) specified in the strSetting
        //                parameter, or the value specified in the strDefault
        //                parameter if the setting is not found.
        //
        //  Parameters:   strSetting - string, name of the setting key to get.
        //
        //                strDefault - string, value to return if the setting
        //                             key is not found.
        //
        //  Returns:      string, the value of the AppSetting, or the default
        //                value if the setting is not found.
        // --------------------------------------------------------------------
        public static string GetAppSetting(string strSetting, string strDefault)
        {
            string strValue = null;

            if (ConfigurationManager.AppSettings[strSetting] != null)
            {
                // Get the setting value.
                strValue = ConfigurationManager.AppSettings[strSetting].ToString();
            }
            else
            {
                // Assign the default value.
                strValue = strDefault;
            }

            return strValue;

        }   // GetAppSetting()
        #endregion


    }   // class AppSettings

}   // namespace OpenSource
