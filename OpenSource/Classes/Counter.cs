﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Web;

namespace OpenSource
{
    public class Counter
    {
        #region .  Constants  .
        // ---------------------------------------------------------------------
        //  Constants
        // ---------------------------------------------------------------------
        private const string DEFAULT_COUNTER_FILE_NAME = "~/App_Data/Counter.txt";
        private const string DEFAULT_FONT_NAME         = "Arial";
        private const string DEFAULT_FONT_SIZE         = "20";

        #endregion


        #region .  CreateBitmapImage()  .
        // ---------------------------------------------------------------------
        //  Routine:      CreateBitmapImage()
        //
        //  Description:  This routine turns the specified text into a bitmap.
        //
        //                The font name and size to use for the text are stored
        //                in the web.config file.  If they are not found, the
        //                defaults of "Arial" and "20" (pixels) will be used.
        //
        //                This idea came from the following URL:
        //
        //   http://p2p.wrox.com/asp-net-1-0-1-1-professional/12820-img-src-aspx-location.html
        //
        //  Parameters:   strText - string, the text to turn into a bitmap.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        public static void CreateBitmapImage(string strText)
        {
            try
            {
                Bitmap oBmpImage = new Bitmap(1, 1);

                int intWidth  = 0;
                int intHeight = 0;

                // Retrieve the font and font size from the web.config file.
                string strFontName = ConfigurationManager.AppSettings["Counter_FontName"];
                string strFontSize = ConfigurationManager.AppSettings["Counter_FontSize"];

                if (strFontName == null) strFontName = DEFAULT_FONT_NAME;
                if (strFontSize == null) strFontSize = DEFAULT_FONT_SIZE;

                // Create the Font object for the image text drawing.
                Font oFont = new Font(strFontName, Convert.ToInt32(strFontSize),
                                      System.Drawing.FontStyle.Bold,
                                      System.Drawing.GraphicsUnit.Pixel);

                // Create a graphics object to measure the text's width and height.
                Graphics oGraphics = Graphics.FromImage(oBmpImage);

                // This is where the bitmap size is determined.
                intWidth  = (int)oGraphics.MeasureString(strText, oFont).Width;
                intHeight = (int)oGraphics.MeasureString(strText, oFont).Height;

                // Create the bmpImage again with the correct size for the text and font.
                oBmpImage = new Bitmap(oBmpImage, new Size(intWidth, intHeight));

                // Add the colors to the new bitmap.
                oGraphics                   = Graphics.FromImage(oBmpImage);
                oGraphics.SmoothingMode     = SmoothingMode.AntiAlias;
                oGraphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                oGraphics.Clear(Color.White);
                oGraphics.DrawString(strText, oFont, new SolidBrush(Color.FromArgb(102, 102, 102)), 0, 0);
                oGraphics.Flush();

                // Save image.
                oBmpImage.Save(HttpContext.Current.Response.OutputStream,
                               System.Drawing.Imaging.ImageFormat.Gif);

                // Clean up.
                oFont.Dispose();
                oGraphics.Dispose();
                oBmpImage.Dispose();
            }
            catch
            { }

        }   // CreateBitmapImage()
        #endregion


        #region .  GetCounter()  .
        // ---------------------------------------------------------------------
        //  Routine:      GetCounter()
        //
        //  Description:  This routine retrieves the counter value from the text
        //                file.
        //
        //  Parameters:   None
        //
        //  Returns:      String, the counter value read from the file.
        // ---------------------------------------------------------------------
        public static string GetCounter()
        {
            int intCounter = 0;

            try
            {
                // Get the counter file name from the web.config.  If not found,
                // use the default file name in the Constant defined above.
                string strCounterFile = ConfigurationManager.AppSettings["Counter_File"];
                if (strCounterFile == null) strCounterFile = DEFAULT_COUNTER_FILE_NAME;

                strCounterFile = HttpContext.Current.Server.MapPath(strCounterFile);

                // Make sure file exists.
                if (File.Exists(strCounterFile))
                {
                    using (var oFileReader = File.OpenText(strCounterFile))
                    {
                        string strCounter = oFileReader.ReadLine();
                        intCounter = Convert.ToInt32(strCounter);
                    }
                }
            }
            catch
            { }

            return intCounter.ToString();

        }   // GetCounter()
        #endregion


        #region .  SetCounter()  .
        // ---------------------------------------------------------------------
        //  Routine:      SetCounter()
        //
        //  Description:  This routine writes the specified counter value to the
        //                text file.
        //
        //  Parameters:   strCounter - string, the counter value to write.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        public static void SetCounter(string strCounter)
        {
            try
            {
                // Get the counter file name from the web.config.  If not found,
                // use the default file name in the Constant defined above.
                string strCounterFile = ConfigurationManager.AppSettings["Counter_File"];
                if (strCounterFile == null) strCounterFile = DEFAULT_COUNTER_FILE_NAME;

                // Construct the full path to the counter file.
                strCounterFile = HttpContext.Current.Server.MapPath(strCounterFile);

                // Update the counter file.  If it does not exists, it will be
                // created; otherwise, the existing file will be overwritten.
                using (var oFileWriter = File.CreateText(strCounterFile))
                {
                    oFileWriter.WriteLine(strCounter);
                }
            }
            catch
            { }

        }   // SetCounter()
        #endregion


    }   // class Counter

}   // namespace OpenSource
