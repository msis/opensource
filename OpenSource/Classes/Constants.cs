﻿using System;

namespace OpenSource
{
    public static class Constants
    {
        // ---------------------------------------------------------------------
        //  Public Constants:
        // ---------------------------------------------------------------------

        #region .  Public Constants  .

        public enum EmailErrorCode
        {
            EmailTemplateFileMissing       = 300,
            EmailMessageBlank              = 301,
            EmailAddressToBlank            = 302,
            EmailAddressFromBlank          = 303,
            EmailSubjectBlank              = 304,
            EmailSubjectTemplateBlank      = 305,
            EmailSmtpServerBlank           = 306,
            EmailFailedSendToAllRecipients = 307,
            EmailFailedSendToRecipient     = 308,
            EmailFailedToSend              = 309
        }

        // Environment Settings:
        public const string APPSETTING_KEY_ENVIRONMENT                       = "Environment";

        public const string DEFAULT_APPSETTING_ENVIRONMENT                   = "PROD";


        // General Application Settings:                                      
        public const string APPSETTING_KEY_APPLICATION_NAME                  = "Application_Name";
        public const string APPSETTING_KEY_APPLICATION_VERSION               = "Application_Version";
        public const string APPSETTING_KEY_LAST_UPDATED                      = "Application_LastUpdated";

        public const string DEFAULT_APPSETTING_LABEL_APP_NAME                = "OpenSource";
        public const string DEFAULT_APPSETTING_LABEL_APP_VERSION             = "Version 2.0.0";
        public const string DEFAULT_APPSETTING_LABEL_LAST_UPDATED            = "05/22/2014";


        // Counter Settings:                                                 
        public const string APPSETTING_KEY_COUNTER_FILE                      = "Counter_File";
        public const string APPSETTING_KEY_COUNTER_FONT_NAME                 = "Counter_FontName";
        public const string APPSETTING_KEY_COUNTER_FONT_SIZE                 = "Counter_FontSize";

        public const string DEFAULT_APPSETTING_COUNTER_FILE                  = "~/App_Data/Counter.txt";
        public const string DEFAULT_APPSETTING_COUNTER_FONT_NAME             = "Arial";
        public const string DEFAULT_APPSETTING_COUNTER_FONT_SIZE             = "20";


        // E-mail Settings and Defaults:                                     
        public const string APPSETTING_KEY_EMAIL_ADDRESS_FROM                = "Email_AddressFrom";
        public const string APPSETTING_KEY_EMAIL_ADDRESS_TO                  = "Email_AddressTo";
        public const string APPSETTING_KEY_EMAIL_IS_EMAIL_ENABLED            = "Email_IsEmailEnabled";
        public const string APPSETTING_KEY_EMAIL_RETRIES                     = "Email_Retries";
        public const string APPSETTING_KEY_EMAIL_SERVER                      = "Email_Server";
        public const string APPSETTING_KEY_EMAIL_SUBJECT_MESSAGE             = "Email_SubjectMessage";
        public const string APPSETTING_KEY_EMAIL_SUBJECT_TEMPLATE            = "Email_SubjectTemplate";
        public const string APPSETTING_KEY_EMAIL_TEMPLATE                    = "Email_EmailTemplate";
        public const string APPSETTING_KEY_EMAIL_TIMEOUT                     = "Email_Timeout";
        public const string APPSETTING_KEY_EMAIL_WAIT_BETWEEN_RETRIES        = "Email_WaitBetweenRetries";

        public const string DEFAULT_APPSETTING_EMAIL_ADDRESS_FROM            = "pleasedonotreply@vt.edu";
        public const string DEFAULT_APPSETTING_EMAIL_ADDRESS_TO              = "randallp@vt.edu";
        public const string DEFAULT_APPSETTING_EMAIL_IS_EMAIL_ENABLED        = "false";
        public const string DEFAULT_APPSETTING_EMAIL_RETRIES                 = "5";
        public const string DEFAULT_APPSETTING_EMAIL_SERVER                  = "smtp.vt.edu";
        public const string DEFAULT_APPSETTING_EMAIL_SUBJECT_MESSAGE         = "An error occured in the OpenSource web site";
        public const string DEFAULT_APPSETTING_EMAIL_SUBJECT_TEMPLATE        = "OpenSource - %FIELD_ENVIRONMENT% (%FIELD_COMPUTER%) - %FIELD_SUBJECT_MESSAGE%";
        public const string DEFAULT_APPSETTING_EMAIL_TEMPLATE                = "EmailTemplate.txt";
        public const string DEFAULT_APPSETTING_EMAIL_TIMEOUT                 = "10000";         // value is in milliseconds
        public const string DEFAULT_APPSETTING_EMAIL_WAIT_BETWEEN_RETRIES    = "5000";          // value is in milliseconds


        // Link Settings (Footer):                                           
        public const string APPSETTING_KEY_LINK_ACCEPTABLE_USE_POLICY        = "Link_AcceptableUsePolicy";
        public const string APPSETTING_KEY_LINK_ACCESSIBILITY                = "Link_Accessibility";
        public const string APPSETTING_KEY_LINK_BOOKSTORE                    = "Link_Bookstore";
        public const string APPSETTING_KEY_LINK_CONTACT_US                   = "Link_ContactUs";
        public const string APPSETTING_KEY_LINK_EQUAL_OPPORTUNITY            = "Link_EqualOpportunity";
        public const string APPSETTING_KEY_LINK_GIVING_TO_VIRGINIA_TECH      = "Link_GivingToVirginiaTech";
        public const string APPSETTING_KEY_LINK_JOBS_AT_VIRGINIA_TECH        = "Link_JobsAtVirginiaTech";
        public const string APPSETTING_KEY_LINK_PRINCIPLES_OF_COMMUNITY      = "Link_PrinciplesOfCommunity";
        public const string APPSETTING_KEY_LINK_PRIVACYS_TATEMENT            = "Link_PrivacyStatement";
        public const string APPSETTING_KEY_LINK_WEBSITE_FEEDBACK             = "Link_WebsiteFeedback";

        public const string DEFAULT_APPSETTING_LINK_OUR_DEPARTMENT           = "http://ccs.w2k{0}.vt.edu";
        public const string DEFAULT_APPSETTING_LINK_ACCEPTABLE_USE_POLICY    = "https://www.vt.edu/acceptable_use.php";
        public const string DEFAULT_APPSETTING_LINK_ACCESSIBILITY            = "https://www.vt.edu/accessibility.php";
        public const string DEFAULT_APPSETTING_LINK_BOOKSTORE                = "https://www.bookstore.vt.edu";
        public const string DEFAULT_APPSETTING_LINK_CONTACT_US               = "https://www.vt.edu/contacts/";
        public const string DEFAULT_APPSETTING_LINK_EQUAL_OPPORTUNITY        = "https://www.vt.edu/equal_opportunity.php";
        public const string DEFAULT_APPSETTING_LINK_GIVING_TO_VIRGINIA_TECH  = "https://www.givingto.vt.edu";
        public const string DEFAULT_APPSETTING_LINK_JOBS_AT_VIRGINIA_TECH    = "https://www.jobs.vt.edu";
        public const string DEFAULT_APPSETTING_LINK_PRINCIPLES_OF_COMMUNITY  = "https://www.vt.edu/principles.php";
        public const string DEFAULT_APPSETTING_LINK_PRIVACY_STATEMENT        = "https://www.vt.edu/privacy.php";
        public const string DEFAULT_APPSETTING_LINK_WEBSITE_FEEDBACK         = "mailto:hokiess@vt.edu";


        // Counter Settings:                                                 
        public const string APPSETTING_KEY_PROJECTS_FILE                     = "Projects_File";

        public const string DEFAULT_APPSETTING_PROJECTS_FILE                 = "~/App_Data/Projects.xml";

        
        // Various messages and RadWindow titles.                            
        public const string MSG_EMAIL_ADDRESS_FROM_BLANK                     = "The email FROM address is blank.";
        public const string MSG_EMAIL_ADDRESS_TO_BLANK                       = "The email TO address is blank.";
        public const string MSG_EMAIL_FAILED_SEND_TO_ALL_RECIPIENTS          = "The email could not be delivered to all recipients.";
        public const string MSG_EMAIL_FAILED_SEND_TO_RECIPIENT               = "The email could not be delivered to the recipient.";
        public const string MSG_EMAIL_FAILED_TO_SEND                         = "Failed to send the email.";
        public const string MSG_EMAIL_MESSAGE_BLANK                          = "The email MESSAGE body is blank.";
        public const string MSG_EMAIL_SMTP_SERVER_BLANK                      = "The email SMTP SERVER is blank.";
        public const string MSG_EMAIL_SUBJECT_BLANK                          = "The email SUBJECT is blank.";
        public const string MSG_EMAIL_TEMPLATE_FILE_NOT_FOUND                = "The email template file (<b>{0}</b>) cannot be found.";

        // Skin:                                                          
        public const string APPSETTING_KEY_SKIN                              = "Telerik.Skin";

        public const string DEFAULT_APPSETTING_SKIN                          = "Sunset";


        // Email errors.
        public const int    RC_ERROR_NUM_EMAIL_TEMPLATE_FILE_MISSING         = 300;
        public const string RC_ERROR_MSG_EMAIL_TEMPLATE_FILE_MISSING         = "The email template file is missing";

        public const int    RC_ERROR_NUM_EMAIL_MESSAGE_BLANK                 = 301;
        public const string RC_ERROR_MSG_EMAIL_MESSAGE_BLANK                 = "The email message body is blank.";

        public const int    RC_ERROR_NUM_EMAIL_TO_ADDRESS_BLANK              = 302;
        public const string RC_ERROR_MSG_EMAIL_TO_ADDRESS_BLANK              = "The email TO address is blank.";

        public const int    RC_ERROR_NUM_EMAIL_FROM_ADDRESS_BLANK            = 303;
        public const string RC_ERROR_MSG_EMAIL_FROM_ADDRESS_BLANK            = "The email FROM address is blank.";

        public const int    RC_ERROR_NUM_EMAIL_SUBJECT_MESSAGE_BLANK         = 304;
        public const string RC_ERROR_MSG_EMAIL_SUBJECT_MESSAGE_BLANK         = "The email SUBJECT MESSAGE is blank.";

        public const int    RC_ERROR_NUM_EMAIL_SUBJECT_TEMPLATE_BLANK        = 305;
        public const string RC_ERROR_MSG_EMAIL_SUBJECT_TEMPLATE_BLANK        = "The email SUBJECT TEMPLATE is blank.";

        public const int    RC_ERROR_NUM_EMAIL_SMTP_SERVER_BLANK             = 306;
        public const string RC_ERROR_MSG_EMAIL_SMTP_SERVER_BLANK             = "The email SMTP SERVER is blank.";

        public const int    RC_ERROR_NUM_EMAIL_FAILED_SEND_TO_ALL_RECIPIENTS = 307;
        public const string RC_ERROR_MSG_EMAIL_FAILED_SEND_TO_ALL_RECIPIENTS = "The email could not be delivered to all recipients.";

        public const int    RC_ERROR_NUM_EMAIL_FAILED_SEND_TO_RECIPIENT      = 308;
        public const string RC_ERROR_MSG_EMAIL_FAILED_SEND_TO_RECIPIENT      = "The email could not be delivered to the recipient.";

        public const int    RC_ERROR_NUM_EMAIL_FAILED_TO_SEND                = 309;
        public const string RC_ERROR_MSG_EMAIL_FAILED_TO_SEND                = "Failure sending email.";


        // Unexpected error - details will be in the Exception object.
        public const int    RC_ERROR_NUM_UNEXPECTED_ERROR                    = 999;
        public const string RC_ERROR_MSG_UNEXPECTED_ERROR                    = "An unexpected error occurred.";

        #endregion

    
    }   // class Constants

}   // namespace OpenSource
