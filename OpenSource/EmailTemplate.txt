;;-----------------------------------------------------------------------------
;; Email template for the Korra_FileSystemWatcher_Service application.
;;
;; Comment lines begin with two semi-colons (;;) and are ignored in the email.
;;
;; The following fields may be used anywhere in the text of the email:
;;
;;    %FIELD_EMAIL_SUBJECT% - read from the App.config
;;    %FIELD_EMAIL_TO%      - read from the App.config
;;    %FIELD_COMPUTER%      - name of computer
;;    %FIELD_TASK_DATA%     - built in the Program::CheckTask() method
;;-----------------------------------------------------------------------------
Subject:    %FIELD_EMAIL_SUBJECT%
To:         %FIELD_EMAIL_TO%


OpenSource Software...

Please contact the administrator of this system if you have any questions concerning this email.

Sincerely,
VTm:sis - Microsoft:  Secure Infrastructure Services
