﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OpenSource.Master" AutoEventWireup="true" CodeBehind="ProcessRegistration.aspx.cs" Inherits="OpenSource.ProcessRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <style type="text/css">
        #main-content
        {
            width: 566px;
        }

        .div_registration
        {
            margin: 0px 10px 0px 10px;
            width: 550px;
        }

        #table_registration
        {
            border: 0px solid blue;
            font-size: 90%;
            margin: 15px;
            width: 95%;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <!-- BEGIN SUB-HEADER -->
    <div id="sub-header">
        <div id="sub-header-inside">
            <!-- BEGIN SUB HEADER BANNER -->
            <a href="Default.aspx">
                <img src="Images/sub_header_small.png"
                     alt="Collaborative Computing Solutions"
                     height="77"
                     title="Collaborative Computing Solutions"
                     width="566"
                     />
            </a> 
            <!-- END SUB HEADER BANNER -->
        </div>
    </div>
    <!-- END SUB-HEADER -->

    <!-- BEGIN MAIN CONTENT -->
    <div id="main-content">
        <div class="div_registration">
            <table id="table_registration" cellpadding="0" cellspacing="0" border="1">
                <tr>
                    <td>
		                <h1>
                            Download Registration:&nbsp;&nbsp;
                            <asp:Label ID="lblProject" runat="server" Text="" />
		                </h1>
                        <asp:Label ID="lblInformation" runat="server" Text="" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphSubContent" runat="server">
</asp:Content>
