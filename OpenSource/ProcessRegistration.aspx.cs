﻿using System;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Xml;

namespace OpenSource
{
    public partial class ProcessRegistration : System.Web.UI.Page
    {
        // ---------------------------------------------------------------------
        // Control Events:
        // ---------------
        //     Page_Load()
        // ---------------------------------------------------------------------

        #region .  Page_Load()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Load()
        //
        //  Description:  This routine is called when the page first loads.  It
        //                changes the "page-area-background" <div> image to the
        //                two-column image.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            string strCompany           = Session["Company"].ToString();
            string strEmailAddress      = Session["EmailAddress"].ToString();
            string strID                = Session["ID"].ToString();
            string strName              = Session["Name"].ToString();

            string strDownloadLinkEmail = String.Empty;
            string strProjectsFile      = Server.MapPath(AppSettings.Projects_File);
            string strTitle             = String.Empty;

            try
            {
                if (!Page.IsPostBack)
                {
                    // Set the background image for the "page-area-background" div
                    // to the two-column format for the content area.
                    HtmlContainerControl myObject;
                    myObject = (HtmlContainerControl)Master.FindControl("page_area_background");
                    if (myObject != null)
                    {
                        myObject.Style.Remove("background");
                        myObject.Style.Add("background", "#F5F5EB url(../Images/pgarea_vnav_subc_2_col.gif) repeat-y 100% 0;");
                    }

                    // Get the project ID and user's email address from the QueryString.
                    //if (Request.QueryString["ID"] == null)
                    if (String.IsNullOrEmpty(strID))
                    {
                        throw new Exception("Project ID is missing.");
                    }
                    else if (String.IsNullOrEmpty(strCompany))
                    {
                        throw new Exception("Company is missing.");
                    }
                    else if (String.IsNullOrEmpty(strEmailAddress))
                    {
                        throw new Exception("Email address is missing.");
                    }
                    else if (String.IsNullOrEmpty(strName))
                    {
                        throw new Exception("Name address is missing.");
                    }
                    else
                    {
                        if (!File.Exists(strProjectsFile))
                        {
                            throw new FileNotFoundException(String.Format("Cannot find the {0} file.", strProjectsFile));
                        }
                        else
                        {
                            XmlDocument oXmlDoc = new XmlDocument();
                            oXmlDoc.Load(strProjectsFile);

                            XmlNode oXmlNode = oXmlDoc.SelectSingleNode(String.Format("Items/Item[@Active='true' and @ID='{0}']", strID));
                            if (oXmlNode == null)
                            {
                                throw new Exception(String.Format("Cannot find Project ID {0}", strID));
                            }
                            else
                            {
                                strTitle = oXmlNode.Attributes["Title"].Value;
                                strDownloadLinkEmail = oXmlNode.Attributes["DownloadLinkEmail"].Value;

                                StringBuilder sb = new StringBuilder();
                                sb.Append(String.Format("<p>An email has been sent to <font color='#0000FF'><b>{0}</b></font>", strEmailAddress));
                                sb.Append(String.Format(" with a link to download the <font color='#0000FF'><b>{0}</b></font> software package.</p>", strTitle));
                                sb.Append("<p>The temporary link will expire at 11:59 pm EST.  After that you will need to re-register to download this software.</p>");
                                lblInformation.Text = sb.ToString();
                            }
                        }
                    }
                }
            }
            catch { }

        }   // Page_Load()
        #endregion


    }   // class ProcessRegistration

}   // namespace OpenSource
