﻿using System;
using System.Web.UI.HtmlControls;

namespace OpenSource
{
    public partial class FileNotFound : System.Web.UI.Page
    {
        // ---------------------------------------------------------------------
        // Control Events:
        // ---------------
        //     Page_Load()
        // ---------------------------------------------------------------------

        #region .  Page_Load()  .
        // ---------------------------------------------------------------------
        //  Routine:      Page_Load()
        //
        //  Description:  This routine is called when the page first loads.  It
        //                changes the "page-area-background" <div> image to the
        //                two-column image.
        //
        //  Parameters:   sender - The sender object.
        //
        //                e      - The event arguments for the sender object.
        //
        //  Returns:      Nothing
        // ---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            // Assign the default background image to the "page-area" div.
            HtmlContainerControl myObject;
            myObject = (HtmlContainerControl)Master.FindControl("page_area_background");
            myObject.Style.Remove("background");
            myObject.Style.Add("background", "#F5F5EB url(../Images/pgarea_vnav_subc_2_col.gif) repeat-y 100% 0;");
            //myObject.Style.Add("width", "405px");

        }   // Page_Load()
        #endregion


    }   // class FileNotFound

}   // namespace OpenSource
